Plug into your creative outlet and flow into free expression. This painting class will begin with a meditation and everyone will set an intention on what they would like to receive and release through this process. Each class will be surrounding a theme and you may choose what to take and what not to take from the theme, it is up to you.

Website : https://consciouscreationsinc.com/